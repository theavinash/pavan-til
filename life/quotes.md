### Skill

Every skill you acquire doubles your odds of success. — Scott Adams

“What lies behind us and what lies before us are tiny matters compared to what lies within us.” — Ralph Waldo Emerson

GRIT is that mix of passion, perseverance, and self-discipline that keeps us moving forward in spite of obstacles. — Daniel Coyle

Skill is the unified force of experience, intellect and passion in their operation. — John Ruskin

