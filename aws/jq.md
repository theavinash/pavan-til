
#####  List of EC2 instances sorted by launch time
```
aws ec2 describe-instances | jq '.["Reservations"]|.[]|.Instances|.[]|.LaunchTime + " " + .InstanceId' | sort -n
```

##### List EC2 instances by tag name and instance id
```
aws ec2 describe-instances --query "Reservations[*].Instances[*]" | jq '.[]|.[]|(if .Tags then (.Tags[]|select(.Key == "Name").Value) else empty end)+", " +.InstanceId'
```
##### List EC2 instances by Public DNSname, KeyName
```
aws ec2 describe-instances --region us-east-1 |jq '.Reservations[].Instances[]|{PublicDnsName, InstanceId,KeyName}'
```
