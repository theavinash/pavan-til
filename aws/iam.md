#### List IAM keys by user
```
for u in $(aws iam list-users  | jq ".Users[].UserName" --raw-output); do 
  aws iam list-access-keys --user $u | jq '.AccessKeyMetadata[] | .UserName + ":" + .AccessKeyId' ; 
done
```
