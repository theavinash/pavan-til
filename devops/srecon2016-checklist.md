### commands per instance
1.  uptime
2.  dmesg -T | tail 
3.  vmstat 1
4.  mpstat -P ALL 1 
5.  pidstat 1 
6.  iostat -xz 1 
7.  free -m 
8.  sar -n DEV 1
9.  sar -n TCP,ETCP 1 
10. top


### source
http://www.slideshare.net/brendangregg/srecon-2016-performance-checklists-for-sres
