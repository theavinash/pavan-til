### one-liners

 - `$ lsof -p 15873` -  list all open files of PID 
 - `$ lsof -u pavan` -  list all open files of PID owned by "pavan"
 - `sudo lsof | grep mongod | grep TCP` - number of open mongodb connections 
 - `sudo lsof -i -P` - list all open ports
 - `lsof -i :8080` - what's running on port 8080
 - `lsof -i` - show all connections with `-i`
 - `lsof -i 6` - show all ipv6 connections
 - `lsof -i TCP` - show all TCP connections
 - `lsof -i@192.168.0.11` - show connections to a specific host
 - `lsof -i -sTCP:LISTEN` - find ports that are awaiting connections
 - `lsof -i -sTCP:ESTABLISHED` - find established connections
 - `kill -9 `lsof -t -u pavan` - kill everything a given user is doing
 - `kill -HUP `lsof -t -c sshd` - kill with HUP
 - `lsof -i @fw.google.com:2150=2180` - Show open connections with a port range
