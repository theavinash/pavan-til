### Size of DBs
```sql
SELECT table_schema                                        "DB Name", 
   Round(Sum(data_length + index_length) / 1024 / 1024, 1) "DB Size in MB" 
FROM   information_schema.tables 
GROUP  BY table_schema; 
```
### MySQL Dump
```sql
mysql -h... -u... -p... -A --skip-column-names -e"SELECT DISTINCT table_schema FROM information_schema.tables WHERE table_schema NOT IN ('information_schema','mysql')" > ListOfDatabases.txt
```
```bash
for DB in `cat ListOfDatabases.txt`
do
    mysqldump -h... -u... -p... --hex-blob --routines --triggers ${DB} | gzip > ${DB}.sql.gz &
done
wait
```

###MySQL backup
mysqldump every DB of your MySQL Server to its own, 7z-compressed file. The provided setup.sh auto-installs/updates the code and makes the script available as a new, simple shell command (zzmysqldump). The project aims to deliver a fully configfile-driven script: no code editing should be necessary!

https://github.com/TurboLabIt/zzmysqldump


### MySQL dump with out locks
``` mysqldump --compress --quick --triggers --routines --lock-tables=false --single-transaction {YOUR_DATABASE_NAME}
```


### compute database size from a mariadb or mysql query syntax
```
SELECT table_schema AS "Database Name", 
ROUND(SUM(data_length + index_length) / 1024 / 1024, 2) AS "Size in (MB)" 
FROM information_schema.TABLES 
GROUP BY table_schema; ```
