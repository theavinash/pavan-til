#Best Practices
Though not an exhaustive list, some of the best practices that you’ll want to include in your API design are to:

* Build on HTTP (actually, HTTPS–see below), using headers and status codes where possible to communicate in a way that machines can quickly interpret.
* Adopt RESTful practices where sensible, especially around naming resources as nouns and using HTTP verbs GET, PUT, POST, and DELETE appropriately.
* Provide data in JSON format, which is lightweight and easy to consume in most modern programming languages. A data format is not a great place to experiment because it provides another hurdle to developer adoption.
* Consider XML if your developers require it (for example, if you have C# or Java consumers, many of its tools expect XML).
* Bake events into your API with subscription webhooks (or REST hooks) so developers can receive the latest data without polling.
* Encourage or require developers to use encrypted connections via HTTPS, so that credentials are not passed in the clear.

